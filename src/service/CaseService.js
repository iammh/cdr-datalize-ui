import axios from 'axios';

import mapdata from "./data";

// let APIBASE = window.location.origin;
let APIBASE = 'http://localhost:1337';
APIBASE += '/datalize/';
let BASE_URL = APIBASE+'/cdr'

const caseService = {

	getCases() {
		return axios.get(BASE_URL + '/cases');
	},
	doSearch(search, caseId) {
		return axios.post(BASE_URL+"/search/cdr", search, {headers: {
        'accept': 'text/plain',
        'Access-Control-Allow-Origin': '*'
      }});

	},
	getCaseDetails(id) {
		return axios.get(BASE_URL + "/calldetails?cid="+ id);;
	},
	getNightCalls(caseId) {
		console.log("Fetching Night Calls for "+ caseId);
		return axios.get(BASE_URL + '/cases/'+ caseId + '/nightcalls');
	},
  getDayCalls(caseId) {
		console.log("Fetching Night Calls for "+ caseId);
		return axios.get(BASE_URL + '/cases/'+ caseId + '/timedcalls?time=day');
	},
	getSuspectedCalls(caseId) {
		console.log("Fetching Suspected Calls for "+ caseId);
		return axios.get(BASE_URL + '/cases/'+ caseId + '/suspects');
	},
  getCellIdDetails(caseId) {
	  return axios.get(BASE_URL + '/celldetails?cid='+caseId);
  },
  getFrequentCalls(caseId) {
	  return axios.get(BASE_URL + '/cases/'+caseId + '/callfrequency');
  },
  getLocationByCell(caseId) {
	  return axios.get(BASE_URL + '/cases/'+ caseId + '/locationbycell');
  },
  getBASEURL() {
	  return APIBASE
  },
  getFrequentCommunication(caseId) {
	  return axios.get(BASE_URL + '/cases/'+ caseId+ '/fcallers');
  },
  getImeiReport(caseId, caller) {
	  return axios.get(BASE_URL + '/cases/'+ caseId+ '/imeireport?caller='+caller)
  },
  getCDRByReceiverAndIMEI(caseId, receiver, imei) {
	  return axios.get(BASE_URL + '/cases/'+ caseId+ '/imei/details?receiver='+receiver+'&imei='+imei)
  },
  getCDRByCallerAndReceiver(caseId, caller, receiver) {
	  return axios.get(BASE_URL + '/cases/'+ caseId + '/cdr/byreceiverandcaller?caller='+caller+'&receiver='+receiver)
  },
  getDetailCase(caseId) {
    return axios.get(BASE_URL + '/cases/case/details?id='+ caseId )
  },
  getSilentCallsByCase(caseId) {
    return axios.get(BASE_URL + '/cases/silentcalls?id='+ caseId )
  },
  getOutgoingCallFrequencyByCase(caseId) {
    return axios.get(BASE_URL + '/cases/outgoingfrequency?id='+ caseId )
  },
  getIncomingCallFrequencyByCase(caseId) {
    return axios.get(BASE_URL + '/cases/incomingfrequency?id='+ caseId )
  },
  getOfficeHourCalls(caseId) {
    return axios.get(BASE_URL + '/cases/'+ caseId + '/timedcalls?time=office');
  },
  getAreaDetails(caseId) {

    return axios.get(BASE_URL + '/cases/distinct/address?id='+caseId);
  },
  getCDRByAddressAndCid(caseId, address) {
    return axios.get(BASE_URL + '/cases/'+caseId+'/detailsbyaddress?token='+address);
  },
  getLinkAnalysis(caseId) {
	  return axios.get(BASE_URL +'/cases/'+caseId+'/analysis')
  },
  getEveningCall(caseId) {
	  return axios.get(BASE_URL + '/cases/'+ caseId + '/timedcalls?time=evening')
  },
  findAllByDurationAndCase(caseId) {
	  return axios.get(BASE_URL + '/cases/'+caseId + '/callduration')
  },
  frequentCallAddress(caseId) {
	  return axios.get(BASE_URL + '/cases/'+caseId + '/frequentcalladdress')
  },
  getInternetActivity(caseId) {
    return axios.get(BASE_URL + '/cases/'+caseId + '/ip/bycase');
  },
    getMapData() {
	    return mapdata;
    }
}


export default caseService
