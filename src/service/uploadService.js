import * as axios from 'axios';

// const BASE_URL = 'http://localhost:1337/datalize/cdr';
const BASE_URL = window.location.origin+'/datalize/cdr';

function upload(formData, caseId, operator) {
  const url = BASE_URL +'/'+caseId+'/upload?operator='+operator;
  return axios.post(url, formData)
  // get data
    .then(x => x.data)
    // add url field
    .then(x => x.map(data => Object.assign({},
      data, { response: data })));
}

function uploadCellId(formData, caseId, operator) {
  const url = BASE_URL +'/'+caseId+'/store/cell?operator='+operator;
  return axios.post(url, formData)
  // get data
    .then(x => x.data)
    // add url field
    .then(x => x.map(data => Object.assign({},
      data, { response: data })));
}

function uploadIP(formData, caseId, operator) {
  const url = BASE_URL +'/'+caseId+'/store/ip?operator='+operator;
  return axios.post(url, formData)
  // get data
    .then(x => x.data)
    // add url field
    .then(x => x.map(data => Object.assign({},
      data, { response: data })));
}

export { upload , uploadCellId, uploadIP}





// function upload(formData) {
//   const photos = formData.getAll('photos');
//   const promises = photos.map((x) => getImage(x)
//     .then(img => ({
//       id: img,
//       originalName: x.name,
//       fileName: x.name,
//       url: img
//     })));
//   return Promise.all(promises);
// }
//
// function getImage(file) {
//   return new Promise((resolve, reject) => {
//     const fReader = new FileReader();
//     const img = document.createElement('img');
//
//     fReader.onload = () => {
//       img.src = fReader.result;
//       resolve(getBase64Image(img));
//     }
//
//     fReader.readAsDataURL(file);
//   })
// }
//
// function getBase64Image(img) {
//   const canvas = document.createElement('canvas');
//   canvas.width = img.width;
//   canvas.height = img.height;
//
//   const ctx = canvas.getContext('2d');
//   ctx.drawImage(img, 0, 0);
//
//   const dataURL = canvas.toDataURL('image/png');
//
//   return dataURL;
// }
//
// export { upload }
